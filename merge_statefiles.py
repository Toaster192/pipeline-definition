#!/usr/bin/env python3

import configparser
import sys


def ensure_section_key_exists(parser, section, key):
    """Make sure a key exists in a section."""
    if key not in parser[section]:
        parser[section][key] = ''
    return parser


def merge_parsers(parser, parser_sec):
    """Merge the fields of two state files"""
    parser = ensure_section_key_exists(parser, 'state', 'jobs')
    parser = ensure_section_key_exists(parser, 'state', 'recipesets')
    parser_sec = ensure_section_key_exists(parser_sec, 'state', 'jobs')
    parser_sec = ensure_section_key_exists(parser_sec, 'state', 'recipesets')

    parser['state']['jobs'] += ' ' + parser_sec['state']['jobs']
    parser['state']['recipesets'] += ' ' + parser_sec['state']['recipesets']

    rc1 = get_rc(parser)
    rc2 = get_rc(parser_sec)

    if rc1 is None or rc2 is None:
        # retcode == 2 is assumed when retcode is missing in either rc file
        sys.exit(2)

    parser['state']['retcode'] = str(max(rc1, rc2))

    return parser


def get_rc(parser):
    """Retrieves value of 'retcode' key from 'state' section.

    Returns:
        value of retcode or None if it's missing
    """
    try:
        retcode = int(parser['state']['retcode'])
    except (KeyError, ValueError):
        retcode = None

    return retcode


def merge_statefiles(rc_file, rc_sec):
    # read input file (rc) using config file parser
    parser = configparser.RawConfigParser()
    parser.read(rc_file)

    # read secondary input file (rc_sec) using config file parser
    parser_sec = configparser.RawConfigParser()
    parser_sec.read(rc_sec)

    parser = merge_parsers(parser, parser_sec)

    # write parser content back to the standard file rc
    with open(rc_file, 'w') as fileh:
        parser.write(fileh)


if __name__ == '__main__':
    rc_file, rc_sec = sys.argv[1], sys.argv[2]
    merge_statefiles(rc_file, rc_sec)
