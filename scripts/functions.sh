#!/bin/bash
# Add any bash functions needed in the pipeline to this file.
set -euo pipefail

# Write a bold green message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in green
function echo_green {
    echo -e "\e[1;32m${1}\e[0m"
}

# Write a bold red message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_red {
    echo -e "\e[1;31m${1}\e[0m"
}

# Write a bold yellow message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_yellow {
    echo -e "\e[1;33m${1}\e[0m"
}

# Use this to execute an application and log the invocation by prepending
# this to an existing command.
function run_logged() {
    bash -x -c '"$@"' -- "$@"
}

# Run kpet's print-test-cases with default parameters, e.g. the cookies file
function kpet_print_test_cases() {
    run_logged testing-venv/bin/kpet --db kpet-db run print-test-cases --cookies ${COOKIE_FILE} "$@"
}

# Run kpet's print-test-cases for SPECIFIC_TREE, ARCH_CONFIG, PATCHES and
# test set = $1, to see if there are any test cases for this test set and
# return 0 if so, return 1 if there's no match.
function kpet_verify_test_group4patches() {
    TEST_SET=$1
    RC=0
    if [ -z "${PATCHES}" ]; then
    # Fail the test if there are no patches
    return 1
    fi
    RESULT=`kpet_print_test_cases -s "$TEST_SET" -t ${SPECIFIC_TREE} -a ${ARCH_CONFIG} ${PATCHES} || RC=$?`
    if [[ $RC -eq 0 && ! -z "$RESULT" ]]; then
    return 0
    else
    return 1
    fi
}

# Create beaker TEMPLATE_FILE for GROUP_NAME of SET_NAMES with PATCH_ARG,
# write stderr to DEST_STDERR.
function generate_kpet_test_group() {
    TEMPLATE_FILE=$1
    GROUP_NAME=$2
    SET_NAMES=$3
    PATCH_ARG=$4
    DEST_STDERR=$5

    run_logged testing-venv/bin/kpet --db kpet-db run generate \
    -v "job_group=$GROUP_NAME" \
    ${TESTS_BEAKER_ZIP_URL:+-v "suites_zip_url=$TESTS_BEAKER_ZIP_URL"} \
    -t ${SPECIFIC_TREE} \
    -k "$KERNEL_URL" -a ${ARCH_CONFIG} -s "$SET_NAMES" \
    -c "$COMPONENTS_RE" \
    -d "cki@gitlab:${CI_PIPELINE_ID} "'##KVER##'"@${CI_COMMIT_REF_NAME} ${ARCH_CONFIG}" \
    -o "$TEMPLATE_FILE" --cookies ${COOKIE_FILE} $PATCH_ARG 2> $DEST_STDERR
}

# Set an rc parameter.
# Args: section name value
function rc_set() {
    crudini --set "$RC_FILE" "$1" "$2" "$3"
}

# Get an rc parameter.
# Args: section name
# Output: value
function rc_get() {
    crudini --get "$RC_FILE" "$1" "$2"
}

# Delete an rc parameter.
# Args: section name
function rc_del() {
    crudini --del "$RC_FILE" "$1" "$2"
}

# Set an rc state parameter.
# Args: name value
function rc_state_set() {
    rc_set state "$1" "$2"
}

# Get an rc state parameter.
# Args: name
# Output: value
function rc_state_get() {
    rc_get state "$1"
}

# Delete an rc state parameter.
# Args: name
function rc_state_del() {
    rc_del state "$1"
}

# Join an array using a delimiter.
function join_by() {
    local IFS="$1"
    shift
    echo "$*"
}

# Join an array with a multi-character delimiter, but ensure that
# delimiter is added to the first item in the array as well. This is
# helpful for adding '--with=' and '--without' to rpmbuild commands.
function join_by_multi() {
    local d=$1
    shift
    printf "%s" "${@/#/$d}"
}

# Build pip wheels and a git archive for each project.
# Args:
#   $1: Base project name (such as 'skt' or 'kpet')
function make_archive_and_wheels {
    pushd ${GIT_BASE}/${1}
        git archive --format=tar -o ${REPO_DIR}/${1}.tar HEAD
        # Build wheels if this is a python project.
        if [[ -f requirements.txt ]] || [[ -f setup.py ]]; then
            echo_green "Building wheels for ${1}"
            # Build a wheel for the base package.
            pip wheel --quiet --wheel-dir ${WHEELHOUSE_DIR} .
            # Build dependencies if a requirements.txt file is present.
            if [[ -f requirements.txt ]]; then
                pip wheel --quiet --wheel-dir ${WHEELHOUSE_DIR} -r requirements.txt
            fi
        fi
    popd
}

# Attempt to clone from the upstream source first. If that fails, fall back
# to our cached clones that are updated every 15 minutes.
# Args:
#   $1: repo's HTTPS URL (without 'https://')
#   $2: directory for cloned repository
function git_clone {
    echo_green "Cloning $1 from upstream"
    if ! git clone --quiet "https://${1}" $2; then
    echo_yellow "Cloning from upstream failed, attempting to clone from cache"
    if ! git clone --quiet "${GIT_CACHE_PATH}/${1}" $2; then
        echo_red "Cloning from cache failed. Cannot continue."
        exit 1
    fi
    fi
}

# Run the reliable git cloner for repos on github.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_github {
    git_clone "github.com/CKI-project/${1}.git" $2
}

# Run the reliable git cloner for repos on gitlab.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_gitlab_com {
    git_clone "gitlab.com/cki-project/${1}.git" $2
}

# Run the reliable git cloner for repos on gitlab.cee.redhat.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_gitlab_cee {
    git_clone "gitlab.cee.redhat.com/cki-project/${1}.git" $2
}

# Get the short tag for a commit in a repository.
# Args:
#   $1: path to the git repository
function get_short_tag {

    # Does the repo directory exist?
    if [[ ! -d $1 ]]; then
        exit 1
    fi

    # Switch to the repo directory.
    pushd $1 > /dev/null

    # Describe the most recent commit.
    TAG=$(git describe --abbrev=0 || true)

    if [[ $TAG =~ ^kernel ]]; then
        # Handle kernel tags in kernel-VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 3-)
    elif [[ $TAG =~ ^[0-9] ]]; then
        # Handle kernel tags in VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 2-)
    else
        # Handle upstream kernel tags, such as v5.0-rc8, or situations where
        # there are no tags at all.
        SHORT_TAG=$(git rev-list --max-count=1 HEAD | cut -b-7)
    fi

    # Return the short tag we generated with a dash prepended.
    echo "-${SHORT_TAG}"

    popd > /dev/null
}
