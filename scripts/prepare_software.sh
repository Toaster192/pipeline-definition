#!/bin/bash
# Prepare and pre-package all of the software needed in the CKI pipeline to
# avoid extra work and network calls later.
set -euo pipefail

# Enable debug output if DEBUG is set to 1
if [[ ${DEBUG:-} == 1 ]]; then
  set -x
fi

# Use the shared functions in this script.
source ${PIPELINE_DEFINITION_DIR}/scripts/functions.sh

# Set the expected name for the software tarball in object storage.
SOFTWARE_OBJECT="pipeline-${CI_PIPELINE_ID}.tar.xz"
SOFTWARE_OBJECT_URL="${MINIO_ENDPOINT}software/${SOFTWARE_OBJECT}"

# Download the prepared software object and untar it.
function get_software_object {
    curl $SOFTWARE_OBJECT_URL | xz -d - | \
        tar --no-same-owner --no-same-permissions --no-xattrs --touch -xf -
}

# If the software has already been prepared, download it instead of preparing
# it again.
if [[ -d "${CI_PROJECT_DIR}/software" ]]; then
    echo_green "✅ Prepared software is already downloaded."
    return
elif curl -s -I --fail ${SOFTWARE_OBJECT_URL} 2>&1 > /dev/null; then
    echo_green "📥 Downloading prepared CKI software."
    for i in {1..5}; do
      if get_software_object; then
        return
      else
        # Delete a partial restore if there was a network problem during
        # download.
        rm -rf software
      fi
    done
fi

echo_green "📦 Packaging CKI pipeline software."

# Create all of the required directories.
GIT_BASE=${CI_PROJECT_DIR}/git
mkdir -p ${GIT_BASE} ${WHEELHOUSE_DIR} ${REPO_DIR}

# Loop over each list of projects to build wheels and archive each repo.
GITHUB_PROJECTS_ARRAY=($GITHUB_PROJECTS)
for PROJECT in "${GITHUB_PROJECTS_ARRAY[@]}"; do
    git_clone_github ${PROJECT} ${GIT_BASE}/${PROJECT}
    make_archive_and_wheels $PROJECT
done

GITLAB_COM_PROJECTS_ARRAY=($GITLAB_COM_PROJECTS)
for PROJECT in "${GITLAB_COM_PROJECTS_ARRAY[@]}"; do
    git_clone_gitlab_com ${PROJECT} ${GIT_BASE}/${PROJECT}
    make_archive_and_wheels $PROJECT
done

GITLAB_CEE_PROJECTS_ARRAY=($GITLAB_CEE_PROJECTS)
for PROJECT in "${GITLAB_CEE_PROJECTS_ARRAY[@]}"; do
    git_clone_gitlab_cee ${PROJECT} ${GIT_BASE}/${PROJECT}
    make_archive_and_wheels $PROJECT
done

# CI systems for certain projects used in the pipeline may pass in their own
# versions of software that they need to use.
#
# The following variables are used:
#
#  kpet_db_targz_url = https://gitlab.cee.redhat.com/<user_or_project>/kpet-db/-/archive/SHA/kpet-db-SHA.tar.gz
#  kpet_pip_url: git+https://github.com/<user_or_project>/kpet.git@BRANCH
#  skt_pip_url: git+https://github.com/<user_or_project>/skt.git@BRANCH
#  pipeline_definition_branch_override: (bare repo and branch names are provided)
#
# When these variables are present, we must:
#
#  1) Check out a different SHA/branch of the repository
#  2) Re-create the git repository archive
#  3) Install any new pip wheels

# Override kpet-db if needed.
if [[ -n "${kpet_db_targz_url:-}" ]]; then
    KPET_DB_SHA=$(basename $kpet_db_targz_url | grep -oP "kpet-db-(\K[0-9a-fA-F]+)")
    echo_yellow "Found kpet-db override for SHA: ${KPET_DB_SHA}"
    pushd ${GIT_BASE}/kpet-db
        git checkout $KPET_DB_SHA
    popd
    make_archive_and_wheels kpet-db
fi

# Override kpet if needed.
if [[ -n "${kpet_pip_url:-}" ]]; then
    echo_yellow "Found kpet override: ${kpet_pip_url}"
    KPET_BRANCH=$(basename $kpet_pip_url | sed 's/^.*@//')
    pushd ${GIT_BASE}/kpet
        git remote add -f -t $KPET_BRANCH kpet_ci $(echo $kpet_pip_url | sed 's/@.*//')
        git checkout $KPET_BRANCH
    popd
    make_archive_and_wheels kpet
fi

# Override skt if needed.
if [[ -n "${skt_pip_url:-}" ]]; then
    echo_yellow "Found skt override: ${skt_pip_url}"
    SKT_BRANCH=$(basename $skt_pip_url | sed 's/^.*@//')
    pushd ${GIT_BASE}/skt
        git remote add -f -t $SKT_BRANCH skt_ci $(echo $skt_pip_url | sed 's/@.*//')
        git checkout $SKT_BRANCH
    popd
    make_archive_and_wheels skt
fi

# pipeline-definition is special, as it allows to clone from a different repo as well
function download_pipeline_definition() {
    pipeline_definition_url=${pipeline_definition_repository_override:-$PIPELINE_DEFINITION_URL}
    pipeline_definition_repo=${pipeline_definition_url#*//}
    git_clone "${pipeline_definition_repo}" "${GIT_BASE}/pipeline-definition"
    pushd "${GIT_BASE}/pipeline-definition"
        git checkout "${pipeline_definition_branch_override:-master}"
    popd
    make_archive_and_wheels pipeline-definition
}
if [[ -n "${pipeline_definition_repository_override:-}" ]]; then
    echo_yellow "Found pipeline-definition override for repository: ${pipeline_definition_repository_override}"
fi
if [[ -n "${pipeline_definition_branch_override:-}" ]]; then
    echo_yellow "Found pipeline-definition override for branch: ${pipeline_definition_branch_override}"
fi
download_pipeline_definition

# Clean up the git clones.
rm -rf $GIT_BASE

# Verify wheels are properly built.
echo_green "Verifying wheels in a virtual environment."
virtualenv ${CI_PROJECT_DIR}/wheel-verify-venv --no-download
wheel-verify-venv/bin/pip install --quiet --no-index \
    --find-links ${WHEELHOUSE_DIR} \
    ${WHEELHOUSE_DIR}/*.whl


# Upload the compressed archive to object storage without touching the disk.
echo_green "Uploading packaged software to object storage."
tar -cf - software/ | xz -z - | \
    aws --endpoint $MINIO_ENDPOINT s3 cp - s3://software/$SOFTWARE_OBJECT
