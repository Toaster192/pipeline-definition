#!/usr/bin/env python3
import sys

mapping = {
    # For some special tree branches like net-next we use special test sets.
    'net-next': 'net',
    'scsi': 'stor',
    'rdma': 'rdma'
}

if __name__ == '__main__':
    branchname = sys.argv[1]
    default = sys.argv[2]

    try:
        print(mapping[branchname])
    except KeyError:
        # pass-through
        print(default)
