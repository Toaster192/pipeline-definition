"""Test merge_statefiles"""

import configparser
import unittest

from merge_statefiles import merge_parsers
from merge_statefiles import get_rc


class TestMergeStatefiles(unittest.TestCase):
    """Test merge_statefiles"""

    MAX_RETCODE = 3

    @staticmethod
    def _rc(retcode):
        parser = configparser.RawConfigParser()
        parser.read_dict({'state': {
            'jobs': '',
            'recipesets': '',
            'retcode': retcode
        }})
        return parser

    def test_merge_statefiles_retcode(self):
        """Test that retcodes are merged correctly"""
        for retcode1 in range(self.MAX_RETCODE + 1):
            for retcode2 in range(self.MAX_RETCODE + 1):
                parser = merge_parsers(self._rc(retcode1), self._rc(retcode2))
                self.assertEqual(parser['state']['retcode'],
                                 str(max(retcode1, retcode2)))

    def test_get_rc(self):
        """Test that retcodes are retrieved correctly"""
        rc0 = {'state': {'retcode': 0}}
        rc1 = {'state': {'retcode': 1}}
        rc_none = {'state': {}}

        # parser/dict has retcode and it's == 0
        self.assertEqual(get_rc(rc0), 0)
        # parser/dict has retcode and it's == 1
        self.assertEqual(get_rc(rc1), 1)

        # parser/dict has no retcode and return None
        self.assertEqual(get_rc(rc_none), None)
