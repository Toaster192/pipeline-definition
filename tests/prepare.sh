#!/bin/bash
set -euo pipefail

# Verify the prepare stage.
function verify_prepare {
  source scripts/functions.sh
  SOFTWARE_OBJECT=pipeline-${CI_PIPELINE_ID}.tar.xz

  echo_green "Checking for object in minio."
  mc ls local/software/${SOFTWARE_OBJECT}

  echo_green "Downloading object and extracting it."
  mc --quiet cp local/software/${SOFTWARE_OBJECT} .
  mkdir /tmp/software_extracted
  tar -xf ${SOFTWARE_OBJECT} -C /tmp/software_extracted

  pushd /tmp/software_extracted
    echo_green "Verifying directories are present."
    test -d software
    test -d software/repos
    test -d software/wheels

    echo_green "Verifying project repos."
    GITHUB_PROJECTS_ARRAY=($GITHUB_PROJECTS)
    for PROJECT in "${GITHUB_PROJECTS_ARRAY[@]}"; do
        test -f software/repos/${PROJECT}.tar
    done
    GITLAB_COM_PROJECTS_ARRAY=($GITLAB_COM_PROJECTS)
    for PROJECT in "${GITLAB_COM_PROJECTS_ARRAY[@]}"; do
        test -f software/repos/${PROJECT}.tar
    done

    echo_green "Verifying wheels."
    virtualenv verify-wheels --no-download
    verify-wheels/bin/pip install --quiet --no-index \
      --find-links software/wheels \
      software/wheels/*.whl
  popd

  rm -rf /tmp/software_extracted ${SOFTWARE_OBJECT}

  echo_green "🥳🤩 SUCCESS!"
}

# Setup temporary minio server.
curl --retry 5 -Lso /usr/local/bin/minio \
  https://dl.min.io/server/minio/release/linux-amd64/minio
chmod +x /usr/local/bin/minio
mkdir -p /tmp/minio
minio server /tmp/minio &

# Setup minio client.
curl --retry 5 -Lso /usr/local/bin/mc \
  https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x /usr/local/bin/mc
mc config host add local ${MINIO_ENDPOINT} ${AWS_ACCESS_KEY_ID} ${AWS_SECRET_ACCESS_KEY} --api s3v4


# Setup software bucket on temporary minio server.
mc mb local/software
mc policy set public local/software
mc cp /usr/local/bin/mc local/software


# Run the prepare stage
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is already present.
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is packaged in object storage
# but not presrnt on the host.
rm -rf software
source scripts/prepare_software.sh
verify_prepare
